/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com_mycompany.lab3;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author informatics
 */
public class checkOXgame {
    
    public checkOXgame() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public  void  TestCheckWinRow1_o_output_true(){
        String[][] table = {{"o","o","o"},{"-","-","-"},{"-","-","-"}};
        String currentPlayer = "o";
        boolean result = ox.checkWin(table,currentPlayer);
        assertEquals(true,result);
    }
    @Test
    public  void  TestCheckWinRow2_o_output_true(){
        String[][] table = {{"-","-","-"},{"o","o","o"},{"-","-","-"}};
        String currentPlayer = "o";
        boolean result = ox.checkWin(table,currentPlayer);
        assertEquals(true,result);
    }@Test
    public  void  TestCheckWinRow3_o_output_true(){
        String[][] table = {{"-","-","-"},{"-","-","-"},{"o","o","o"}};
        String currentPlayer = "o";
        boolean result = ox.checkWin(table,currentPlayer);
        assertEquals(true,result);
    }@Test
    public  void  TestCheckWinRow1_x_output_true(){
        String[][] table = {{"x","x","x"},{"-","-","-"},{"-","-","-"}};
        String currentPlayer = "x";
        boolean result = ox.checkWin(table,currentPlayer);
        assertEquals(true,result);
    }
    @Test
    public  void  TestCheckWinRow2_x_output_true(){
        String[][] table = {{"-","-","-"},{"x","x","x"},{"-","-","-"}};
        String currentPlayer = "x";
        boolean result = ox.checkWin(table,currentPlayer);
        assertEquals(true,result);
    }@Test
    public  void  TestCheckWinRow3_x_output_true(){
        String[][] table = {{"-","-","-"},{"-","-","-"},{"x","x","x"}};
        String currentPlayer = "x";
        boolean result = ox.checkWin(table,currentPlayer);
        assertEquals(true,result);
    }@Test
    public  void  TestCheckWinObl1_o_output_true(){
        String[][] table = {{"o","-","-"},{"-","o","-"},{"-","-","o"}};
        String currentPlayer = "o";
        boolean result = ox.checkWin(table,currentPlayer);
        assertEquals(true,result);
    }
    @Test
    public  void  TestCheckWinObl2_o_output_true(){
        String[][] table = {{"-","-","o"},{"-","o","-"},{"o","-","-"}};
        String currentPlayer = "o";
        boolean result = ox.checkWin(table,currentPlayer);
        assertEquals(true,result);
    }@Test
    public  void  TestCheckWinObl1_x_output_true(){
        String[][] table = {{"x","-","-"},{"-","x","-"},{"-","-","x"}};
        String currentPlayer = "x";
        boolean result = ox.checkWin(table,currentPlayer);
        assertEquals(true,result);
    }@Test
    public  void  TestCheckWinObl2_x_output_true(){
        String[][] table = {{"-","-","x"},{"-","x","-"},{"x","-","-"}};
        String currentPlayer = "x";
        boolean result = ox.checkWin(table,currentPlayer);
        assertEquals(true,result);
    }@Test
    public void testCheckdraw_false(){
        String[][] table = ox.getTable();
        boolean result = ox.checkDraw(table);
        assertEquals(true,result);
    }

}
